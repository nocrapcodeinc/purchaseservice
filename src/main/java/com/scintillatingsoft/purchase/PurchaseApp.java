package com.scintillatingsoft.purchase;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class PurchaseApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseApp.class);

    public static void main(String[] args) {
        SpringApplication.run(PurchaseApp.class, args);
        LOGGER.debug("\n------------------ PurchaseService up and running ---------------------");
    }
}
