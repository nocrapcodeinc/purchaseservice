package com.scintillatingsoft.purchase;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private Integer errorCode;
    private String message;

    public ErrorResponse(Integer errorCode, String message) {
        this.setErrorCode(errorCode);
        this.setMessage(message);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
