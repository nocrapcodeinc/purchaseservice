package com.scintillatingsoft.purchase;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

interface PurchaseRepository extends PagingAndSortingRepository<Purchase, Integer> {

    List<Purchase> findByUser(String user);
}
