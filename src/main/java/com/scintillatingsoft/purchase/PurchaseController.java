package com.scintillatingsoft.purchase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@RestController
@Transactional
public class PurchaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseController.class);

    @Autowired
    private PurchaseRepository repository;

    @RequestMapping(value = "/purchases/user/{user}", method = RequestMethod.GET)
    public List<Purchase> findByUser(@PathVariable String user) {
        try {
            LOGGER.debug("Fetching purchases for user: " + user);
            return repository.findByUser(user);
        } catch (Exception e) {
            LOGGER.error("Error while fetching purchases for user: " + user, e);
            throw new RuntimeException("Error while fetching purchases.");
        }
    }

    @GetMapping(value = "/hello")
    public String hello(){
        return "Hello from service";
    }

    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    public ResponseEntity<String> save(@RequestBody Purchase purchase) {
        try {
            LOGGER.debug("Persisting purchase: " + purchase.toString());
            purchase.setPurchased(new Date());
            repository.save(purchase);
            return ResponseEntity.ok("{}");
        } catch (Exception e) {
            LOGGER.error("Error while persisting purchase: " + purchase.toString(), e);
            throw new RuntimeException("Error while persisting purchase.");
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.OK);
    }
}
